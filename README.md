## Ejercicio - Identificando una secuencia de caracteres

Define el método `find_symbols` que recibe como argumento un string e identifica una secuencia determinada de símbolos en el string. Todas las comparaciones en el `driver code` debe ser `true`. Es importante que observes la secuencia que sigue cada caracter en el string.


```ruby
#find_symbols method






#driver code
p find_symbols("-m+=10=+s+") == true
p find_symbols("h++l+") == false
p find_symbols("lk-+jh") == false
p find_symbols("-+hello") == false
p find_symbols("-d+0=sa-z-") == true
p find_symbols("--+0=sa-z-") == false
p find_symbols("-++0=sa-z-") == false
p find_symbols("-8+0=sa-z-") == false
```